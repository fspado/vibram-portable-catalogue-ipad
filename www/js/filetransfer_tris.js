function FileTransferForDesktop(parent)
{
	var TIMEOUT = 15000;
	
	
	this.download = function (source, target, saveImageSuccess, saveImageError, fileName)
	{
		console.log('function download');
		var xhr = new XMLHttpRequest();
		console.log('Object XHR created');
		xhr.open('GET', source, true);
		xhr.responseType = 'arraybuffer';
		setTimeout(function () {     /* vs. a.timeout */
			if (xhr.readyState < 4) {
				xhr.abort();
			}
		}, 5);            /* vs. a.ontimeout */
		
		function abort(xhr) 
		{
			console.log('Timeout ' + source);
			console.log("Request Timeout\nFailed to access " + source);
			xhr.abort();
			saveImageError();
		}
		
		xhr.onload = function(e) 
		{
			console.log('Onload ' + source);
			if (this.status == 200) 
			{
				BlobBuilder = window.MozBlobBuilder || window.WebKitBlobBuilder || window.BlobBuilder;
				var bb = new BlobBuilder();
				bb.append(this.response); // Note: not xhr.responseText

				var blob = bb.getBlob();
				dataStore(fileName, parent, blob, saveImageSuccess, saveImageError);
			}
			else
			{
			console.log('Impossible to download file ' + fileName + ': status is ' + this.status);
			saveImageError();
			}
		};
		
		xhr.onerror = function(e)
		{
			console.log('Impossible to download file ' + fileName + '. HtmlHttpError');
			saveImageError();
		}
		
		console.log('Before send ' + source);
		xhr.send();
		setTimeout(function()
		{
			abort(xhr);
		}, 
		TIMEOUT);
		
		console.log('After send ' + source);
	}	
		
		
	function dataStore(fileName, parent, blob, saveImageSuccess, saveImageError)
	{
		console.log('function dataStore');
		parent.getFile(fileName, {create: true, exclusive: false}, function(fileEntry) 
		{
			console.log('Downloading ' + parent.fullPath + fileName + '...');
			fileEntry.createWriter(function(writer) 
			{  // FileWriter
            
				writer.onwrite = function(e) 
				{
				    console.log('File '+ parent.fullPath + fileName + ' succesfully writed.');
				};
            
				writer.onerror = function(e)
				{
				  console.log('Write failed: ' + e);
				  saveImageError();
				};
            
				writer.write(blob);
				saveImageSuccess(fileEntry);
            
			},
			function(err)
			{
				console.log(err.code);
				saveImageError(err);
			});
		});
	}
}
	
	
